#!/bin/bash
# publish.sh


[[ ! -d  $HOME/ii-reveal/publish ]] && mkdir -p $HOME/ii-reveal/publish
docker run --name emacs-reveal-publish-$USER \
           --rm -v $(pwd):/source \
           -v $HOME/ii-reveal:/home/ii-reveal \
           -u $(id -u ${USER}):$(id -g ${USER}) \
           -e HOME='/home/emacs' \
           registry.gitlab.com/heyste/docker/emacs-reveal \
           /bin/bash -c "cd /home/ii-reveal ; \
                         git clone https://gitlab.com/heyste/emacs-reveal-howto.git ; \
                         ls -l /home/ii-reveal ; \
                         cd /home/ii-reveal/publish ; \
                         cp /source/*.org . ; cp /source/*.html . ; \
                         emacs --batch --load ../emacs-reveal-howto/elisp/publish.el ; \
                         rm -rf /home/ii-reveal/emacs-reveal-howto ; \
                         find /home/ii-reveal/publish -type f -name '*~' -delete ; \
                         ls -l  /home/ii-reveal/publish/public"
cd $HOME/ii-reveal/publish/public
python -m SimpleHTTPServer 8000
